import 'package:SkypeClone/resources/firebase_repository.dart';
import 'package:SkypeClone/screens/home_screen.dart';
import 'package:SkypeClone/screens/login_screen.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  FirebaseRepository _repository = FirebaseRepository();

  @override
  Widget build(BuildContext context) {
    // Firestore.instance
    //     .collection("users")
    //     .document()
    //     .setData({"name": "houinkyouma"});

    // print(_repository.getCurrentUser());
    return MaterialApp(
      title: "Skype Clone Training",
      debugShowCheckedModeBanner: false,
      home: FutureBuilder(
        future: _repository.getCurrentUser(),
        builder: (context, snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.none:
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator(),
              );
            case ConnectionState.active:
            case ConnectionState.done:
              if (snapshot.hasError)
                return Text(
                    "FutureBuilder-snapshot.hasError Something is wrong!");
              if (snapshot.hasData) {
                print("check FutureBuilder hasData");
                // _repository.signOut();

                return HomeScreen();
              }
              print("check FutureBuilder LoginScreen");
              return LoginScreen();

            default:
              return Text("FutureBuilder-default Something is wrong!");
          }
        },
      ),
    );
  }
}
