import 'package:SkypeClone/resources/firebase_repository.dart';
import 'package:SkypeClone/screens/login_screen.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(children: <Widget>[
          Text("Home Screen"),
          FlatButton(
            padding: EdgeInsets.all(35),
            child: Text("Logout",
                style: TextStyle(
                    fontSize: 35,
                    fontWeight: FontWeight.w900,
                    letterSpacing: 1.2)),
            onPressed: () async {
              await FirebaseRepository().signOut();
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                    builder: (context) => LoginScreen(),
                  ));
            },
          ),
        ]),
      ),
    );
  }
}
